package tabula_opera

import (
	"fmt"
	"io"
	"io/ioutil"
	"strconv"
	"strings"
	"time"
)

import "os"

func Parse(r io.Reader) (records []TimeRecord, err error) {
	var items []item
	i, err := ioutil.ReadAll(r)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		return
	}
	input := string(i)
	l := lex("input", input)
	for {
		item := l.nextItem()
		items = append(items, item)
		if item.typ == itemEOF || item.typ == itemError {
			break
		}
	}

	year, month, day := time.Now().Date()
	cur := NewTimeRecord()

	for _, item := range items {
		switch t := item.typ; {
		case t == itemDate:
			p, e := time.Parse("2006-01-02", item.val)
			if e != nil {
				fmt.Fprintf(os.Stderr, "Date parse error: %v\n", e)
			} else {
				year, month, day = p.Date()
			}
			break
		case t == itemTimeBegin:
			h, m, e := parseTime(item.val)
			if e != nil {
				fmt.Fprintf(os.Stderr, "Time parse error: %v\n", e)
			} else {
				cur.start = time.Date(year, month, day, h, m, 0, 0, time.Local)
			}
			break
		case t == itemTimeEnd:
			h, m, e := parseTime(item.val)
			if e != nil {
				fmt.Fprintf(os.Stderr, "Time parse error: %v\n", e)
			} else {
				cur.end = time.Date(year, month, day, h, m, 0, 0, time.Local)
			}
			break
		case t == itemPercent:
			i, e := strconv.Atoi(strings.Replace(item.val, "%", "", 1))
			if e != nil {
				fmt.Fprintf(os.Stderr, "Percent parse error: %v\n", e)
				i = 100
			}
			if i < 0 {
				i = 0
			}
			if i > 100 {
				i = 100
			}
			cur.percent = float32(i) / 100
			break
		case t == itemDescription:
			cur.description = strings.TrimSpace(item.val)
			break
		case t == itemOpenTag:
			break
		case t == itemTag:
			cur.tags = append(cur.tags, item.val)
			break
		case t == itemCloseTag:
			break
		case t == itemRecordBegin:
			if !cur.start.IsZero() {
				records = append(records, cur)
				cur = NewTimeRecord()
			}
			break
		case t == itemRecordEnd:
			records = append(records, cur)
			cur = NewTimeRecord()
			break
		}

	}
	return
}

func parseTime(s string) (hour, minutes int, err error) {
	if len(s) != 4 {
		err = fmt.Errorf("invalid time string: '%s'", s)
		return
	}
	hour, err = strconv.Atoi(s[:2])
	minutes, err = strconv.Atoi(s[2:])
	return
}
