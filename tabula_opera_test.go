package tabula_opera

import (
	"bytes"
	"testing"
)

func TestFormat(t *testing.T) {
	src := bytes.NewBuffer([]byte("Date: 2012-06-23\n"))
	dst := new(bytes.Buffer)
	if err := Format(src, dst); err != nil {
		t.Errorf("Error; %v", err)
	}
	t.Logf("Output: %v", dst)
}
