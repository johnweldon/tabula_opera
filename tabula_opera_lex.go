package tabula_opera

import (
	"fmt"
	"os"
	"strings"
	"unicode/utf8"
)

type item struct {
	typ itemType
	val string
}

func (i item) String() string {
	switch {
	case i.typ == itemError:
		return fmt.Sprintf("error: %s", i.val)
	}
	return fmt.Sprintf("%s:%q", i.typ, i.val)
}

type itemType int

const (
	itemError itemType = iota
	itemEOF
	itemText
	itemDatePrefix
	itemDate
	itemRecordBegin
	itemTimeBegin
	itemTimeEnd
	itemPercent
	itemDescription
	itemOpenTag
	itemCloseTag
	itemTag
	itemRecordEnd
)

var itemName = map[itemType]string{
	itemError:       "error",
	itemEOF:         "EOF",
	itemText:        "text",
	itemDatePrefix:  "date prefix",
	itemDate:        "date",
	itemRecordBegin: "begin time record",
	itemTimeBegin:   "begin time",
	itemTimeEnd:     "end time",
	itemPercent:     "percent",
	itemDescription: "description",
	itemOpenTag:     "open tag",
	itemCloseTag:    "close tag",
	itemTag:         "tag",
	itemRecordEnd:   "end time record",
}

func (i itemType) String() string {
	s := itemName[i]
	if s == "" {
		return fmt.Sprintf("item%d", int(i))
	}
	return s
}

const eof = -1
const datepfx = "Date:"
const eol = '\n'

type stateFn func(*lexer) stateFn

type lexer struct {
	name  string
	input string
	state stateFn
	pos   int
	start int
	width int
	items chan item
}

func (l *lexer) hasText() bool {
	return l.pos > l.start
}

func (l *lexer) text() string {
	return l.input[l.start:l.pos]
}

func (l *lexer) next() (r rune) {
	if l.pos >= len(l.input) {
		l.width = 0
		return eof
	}
	r, l.width = utf8.DecodeRuneInString(l.input[l.pos:])
	l.pos += l.width
	return r
}

func (l *lexer) peek() rune {
	r := l.next()
	l.backup()
	return r
}

func (l *lexer) backup() {
	l.pos -= l.width
}

func (l *lexer) emit(t itemType) {
	l.items <- item{t, l.text()}
	l.start = l.pos
}

func (l *lexer) emitIfHasText(t itemType) {
	if l.hasText() {
		l.emit(t)
	}
}

func (l *lexer) reset() {
	l.pos = l.start
}

func (l *lexer) ignore() {
	l.start = l.pos
}

func (l *lexer) accept(valid string) bool {
	if strings.IndexRune(valid, l.next()) >= 0 {
		return true
	}
	l.backup()
	return false
}

func (l *lexer) acceptN(valid string, count int) bool {
	for i := 0; i < count; i++ {
		if !l.accept(valid) {
			return false
		}
	}
	return true
}

func (l *lexer) acceptRun(valid string) {
	for strings.IndexRune(valid, l.next()) >= 0 {
	}
	l.backup()
}

func (l *lexer) lineNumber() int {
	return 1 + strings.Count(l.input[:l.pos], "\n")
}

func (l *lexer) errorf(format string, args ...interface{}) stateFn {
	l.items <- item{itemError, fmt.Sprintf(format, args...)}
	return nil
}

func (l *lexer) debug() {
	fmt.Fprintf(os.Stderr, "DEBUG: start - %d, pos - %d, str - '%s'\n", l.start, l.pos, l.text())
}

func (l *lexer) nextItem() item {
	for {
		select {
		case item := <-l.items:
			return item
		default:
			l.state = l.state(l)
		}
	}
	panic("not reached")
}

func lex(name, input string) *lexer {
	l := &lexer{
		name:  name,
		input: input,
		state: lexInput,
		items: make(chan item, 2),
	}
	return l
}

/*
 */

func lexInput(l *lexer) stateFn {
	l.acceptRun(whiteSpace)
	l.ignore()
	return lexNewLine
}

func lexNewLine(l *lexer) stateFn {
	if strings.HasPrefix(l.input[l.pos:], datepfx) {
		l.pos += len(datepfx)
		l.emit(itemDatePrefix)
		return lexDateLine
	}
	if l.accept("012") {
		l.backup()
		return lexTimeBegin
	}
	return lexText
}

func lexText(l *lexer) stateFn {
	for {
		switch r := l.next(); {
		case r == eof:
			l.ignore()
			l.emit(itemEOF)
			return nil
		case r == eol:
			l.ignore()
			return lexNewLine
		}
	}
	return l.errorf("shouldn't get here")
}

func lexIgnoreRestOfLine(l *lexer) stateFn {
	for {
		switch r := l.next(); {
		case r == eof:
			l.ignore()
			l.emit(itemEOF)
			return nil
		case r == eol:
			l.ignore()
			return lexNewLine
		}
	}
	return l.errorf("shouldn't get here")
}

func lexDateLine(l *lexer) stateFn {
	switch r := l.next(); {
	case ('0' <= r && r <= '9'):
		l.backup()
		return lexDate
	case r == eof || r == eol:
		return l.errorf("incomplete date line")
	case isSpace(r):
		l.ignore()
	default:
		return l.errorf("unrecognized character in date line: %#U", r)
	}
	return lexDateLine
}

const digits = "0123456789"
const whiteSpace = " \t\n\r"

func lexDate(l *lexer) stateFn {
	if !(l.acceptN(digits, 4) && /* year */
		l.accept("-") &&
		l.accept("01") && l.accept(digits) && /* month */
		l.accept("-") &&
		l.accept("0123") && l.accept(digits) /* day */) {
		return l.errorf("invalid date format (yyyy-MM-dd) in date line: '%s'", l.text())
	}
	l.emitIfHasText(itemDate)
	return lexIgnoreRestOfLine
}

func lexTimeBegin(l *lexer) stateFn {
	if !(l.accept("012") && l.accept(digits) && l.accept("012345") && l.accept(digits) && /* HHmm */
		l.accept("-") &&
		l.accept("012") && l.accept(digits) && l.accept("012345") && l.accept(digits) && /* HHmm */
		l.accept(whiteSpace)) {
		l.reset()
		return lexText
	}
	l.reset()
	l.emit(itemRecordBegin)

	if !(l.accept("012") && l.accept(digits) && l.accept("012345") && l.accept(digits)) {
		return l.errorf("invalid start time format (HHmm) in start time: '%s'", l.text())
	}
	l.emitIfHasText(itemTimeBegin)

	l.accept("-")
	l.ignore()

	return lexTimeEnd
}

func lexTimeEnd(l *lexer) stateFn {
	if !(l.accept("012") && l.accept(digits) && l.accept("012345") && l.accept(digits)) {
		return l.errorf("invalid end time format (HHmm) in start time: '%s'", l.text())
	}
	l.emitIfHasText(itemTimeEnd)
	return lexFindPercent
}

func lexFindPercent(l *lexer) stateFn {
	l.acceptRun(" \t")
	l.ignore()
	switch r := l.next(); {
	case r == eof || r == eol:
		l.backup()
		return lexDescription
	case isDigit(r):
		l.backup()
		return lexPercent
	default:
		l.backup()
		return lexDescription
	}
	return l.errorf("should never get here (find percent)")
}

func lexPercent(l *lexer) stateFn {
	for i := 0; i < 3; i++ {
		switch r := l.next(); {
		case isDigit(r):
			break
		case r == '%':
			l.emitIfHasText(itemPercent)
			return lexDescription
		case r == eof || r == eol:
			l.reset()
			return lexDescription
		default:
			return l.errorf("invalid percent section: '%s'", l.text())
		}
	}
	return l.errorf("should never get here (in percent) '%s'", l.text())
}

func lexDescription(l *lexer) stateFn {
	for {
		switch r := l.next(); {
		case r == eol || r == eof:
			l.backup()
			l.emitIfHasText(itemDescription)
			l.emit(itemRecordEnd)
			return lexText
		case r == '#':
			l.backup()
			l.emitIfHasText(itemDescription)
			return lexOpenTag
		}
	}
	l.emit(itemEOF)
	return nil
}

func lexOpenTag(l *lexer) stateFn {
	if !(l.acceptN("#", 2)) {
		return l.errorf("invalid open tag: '%s'", l.text())
	}
	l.emit(itemOpenTag)
	return lexTags
}

func lexCloseTag(l *lexer) stateFn {
	if !(l.acceptN("#", 2)) {
		return l.errorf("invalid close tag: '%s'", l.text())
	}
	l.emit(itemCloseTag)
	l.emit(itemRecordEnd)
	return lexIgnoreRestOfLine
}

func lexTags(l *lexer) stateFn {
	for {
		switch r := l.next(); {
		case r == '#':
			l.backup()
			l.emitIfHasText(itemTag)
			return lexCloseTag
		case isSpace(r):
			l.backup()
			l.emitIfHasText(itemTag)
			l.next()
			l.ignore()
			return lexTags
		case r == eof || r == eol:
			return l.errorf("unclosed tag section: '%s'", l.text())
		}
	}
	return l.errorf("should not get here (in tags)")
}

func isDigit(r rune) bool {
	return '0' <= r && r <= '9'
}

func isSpace(r rune) bool {
	switch r {
	case ' ', '\t', '\n', '\r':
		return true
	}
	return false
}
