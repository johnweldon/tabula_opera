package tabula_opera

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"os"
	"strings"
	"time"
)

type TimeRecord struct {
	start       time.Time
	end         time.Time
	percent     float32
	description string
	tags        []string
}

func (r *TimeRecord) GetStart() time.Time    { return r.start }
func (r *TimeRecord) GetEnd() time.Time      { return r.end }
func (r *TimeRecord) GetDescription() string { return r.description }
func (r *TimeRecord) GetTags() []string      { return r.tags }

func (r *TimeRecord) GetDuration() time.Duration {
	return time.Duration(float64(r.end.Sub(r.start)) * float64(r.percent))
}
func (r TimeRecord) String() string {
	start := r.start.Format("15:04")
	end := r.end.Format("15:04")
	pct := int(r.percent * 100)
	hours := int(r.GetDuration().Hours())
	minutes := int(math.Mod(r.GetDuration().Minutes(), 60))

	return fmt.Sprintf("%s-%s    %d:%d (%d%%) %s\n", start, end, hours, minutes, pct, r.description)
}

func NewTimeRecord() TimeRecord {
	r := new(TimeRecord)
	r.percent = 1
	return *r
}

type TimeReport struct {
	records []TimeRecord
}

func (r TimeReport) String() string {
	records := r.GetRecordsByDay()
	s := make([]string, len(records))
	ix := 0
	for rec := range records {
		s[ix] = rec.String() + "HI"
		ix++
	}
	return strings.Join(s, "\n")
}

func Format(reader io.Reader, writer io.Writer) error {
	records, err := Parse(reader)
	if err != nil {
		return err
	}
	report := NewTimeReport(records)
	fmt.Fprintf(writer, "%s", report)
	return nil
}

func ReadLog(path string) TimeReport {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error reading file '%s': %q", path, err)
		return TimeReport{}
	}
	records, err := Parse(bytes.NewBuffer(data))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error reading file '%s': %q", path, err)
		return TimeReport{}
	}
	return NewTimeReport(records)
}

func NewTimeReport(records []TimeRecord) TimeReport {
	return TimeReport{records}
}

func (r *TimeReport) GetTags() []string {
	tags := make(map[string][]string)
	for _, rec := range r.records {
		for _, t := range rec.tags {
			tags[t] = rec.tags
		}
	}
	var ret []string
	for t := range tags {
		ret = append(ret, t)
	}
	return ret
}

func (r *TimeReport) GetTagGroups() [][]string {
	groups := make(map[string][]string)
	for _, rec := range r.records {
		key := strings.Join(rec.tags, " ")
		groups[key] = rec.tags
	}
	var ret [][]string
	for _, v := range groups {
		ret = append(ret, v)
	}
	return ret
}

func (r *TimeReport) GetDates() []time.Time {
	days := make(map[string]date)
	for _, rec := range r.records {
		str := rec.start.Format("2006-01-02")
		val := date{rec.start.Year(), rec.start.Month(), rec.start.Day()}
		days[str] = val
	}
	var ret []time.Time
	for _, v := range days {
		ret = append(ret, time.Date(v.y, v.m, v.d, 0, 0, 0, 0, time.Local))
	}
	return ret
}

func (r *TimeReport) GetRecordsWithTag(tag string) []TimeRecord {
	return Filter(r.records, thatContainTag(tag))
}

func (r *TimeReport) GetRecordsWithTags(tags []string) []TimeRecord {
	return Filter(r.records, thatHaveAllTags(tags))
}

func (r *TimeReport) GetRecordsOnDay(dtt time.Time) []TimeRecord {
	dt := date{dtt.Year(), dtt.Month(), dtt.Day()}
	return Filter(r.records, thatAreOnDate(dt))
}

func (r *TimeReport) GetRecordsByDay() map[time.Time][]TimeRecord {
	dts := r.GetDates()
	ret := make(map[time.Time][]TimeRecord)
	for _, dt := range dts {
		recs := r.GetRecordsOnDay(dt)
		ret[dt] = recs
	}
	return ret
}

func (r *TimeReport) GetRecordsByTag() map[string][]TimeRecord {
	tags := r.GetTags()
	ret := make(map[string][]TimeRecord)
	for _, tag := range tags {
		ret[tag] = r.GetRecordsWithTag(tag)
	}
	return ret
}

func Filter(r []TimeRecord, predicate recordPredicate) []TimeRecord {
	var filtered []TimeRecord
	for _, i := range r {
		if predicate(i) {
			filtered = append(filtered, i)
		}
	}
	return filtered
}

// Private

type recordPredicate func(TimeRecord) bool
type date struct {
	y int
	m time.Month
	d int
}

func thatContainTag(tag string) recordPredicate {
	return func(r TimeRecord) bool {
		for _, i := range r.tags {
			if i == tag {
				return true
			}
		}
		return false
	}
}

func thatHaveAllTags(tags []string) recordPredicate {
	var predicates []recordPredicate
	return func(r TimeRecord) bool {
		pass := true
		for _, pred := range predicates {
			pass = pass && pred(r)
		}
		return pass
	}
}

func thatAreOnDate(dt date) recordPredicate {
	start := time.Date(dt.y, dt.m, dt.d, 0, 0, 0, 0, time.Local)
	end := time.Date(dt.y, dt.m, dt.d, 23, 59, 59, 9999, time.Local)
	return func(r TimeRecord) bool {
		return r.start.After(start) && r.start.Before(end)
	}
}
