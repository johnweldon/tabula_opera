package tabula_opera

import (
	"reflect"
	"testing"
)

type test struct {
	name  string
	input string
	items []item
}

var tests = []test{
	{"basic", "\nDate: 2012-06-13\n\n2000-1200 13% this is text ##TAG1 TAG2##  \n", []item{
		{itemDatePrefix, "Date:"},
		{itemDate, "2012-06-13"},
		{itemRecordBegin, ""},
		{itemTimeBegin, "2000"},
		{itemTimeEnd, "1200"},
		{itemPercent, "13%"},
		{itemDescription, " this is text "},
		{itemOpenTag, "##"},
		{itemTag, "TAG1"},
		{itemTag, "TAG2"},
		{itemCloseTag, "##"},
		{itemRecordEnd, ""},
		{itemEOF, ""},
	}},
	{"basic2", "Date: 2012-06-13\n\n2000-1200  this is text ##TAG1 TAG2##  \n", []item{
		{itemDatePrefix, "Date:"},
		{itemDate, "2012-06-13"},
		{itemRecordBegin, ""},
		{itemTimeBegin, "2000"},
		{itemTimeEnd, "1200"},
		{itemDescription, "this is text "},
		{itemOpenTag, "##"},
		{itemTag, "TAG1"},
		{itemTag, "TAG2"},
		{itemCloseTag, "##"},
		{itemRecordEnd, ""},
		{itemEOF, ""},
	}},
	{"notes", "asdf\nasdf\nasdf ", []item{
		{itemEOF, ""},
	}},
	{"junk", "asdf\nasdf\nasdf \n 0800-0900 \n0900-1000\n\nTotal", []item{
		{itemRecordBegin, ""},
		{itemTimeBegin, "0900"},
		{itemTimeEnd, "1000"},
		{itemRecordEnd, ""},
		{itemEOF, ""},
	}},
}

func collect(t *test) (items []item) {
	l := lex(t.name, t.input)
	for {
		item := l.nextItem()
		items = append(items, item)
		if item.typ == itemEOF || item.typ == itemError {
			break
		}
	}
	return
}

func TestLex(t *testing.T) {
	for _, test := range tests {
		items := collect(&test)
		if !reflect.DeepEqual(items, test.items) {
			t.Errorf("%s: got\n\t%v\nexpected\n\t%v", test.name, items, test.items)
		}
	}
}
