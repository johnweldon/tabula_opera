package tabula_opera

import (
	"reflect"
	"strings"
	"testing"
	"time"
)

type parseTest struct {
	name    string
	input   string
	records []TimeRecord
}

var parseTests = []parseTest{
	{
		"Simple",
		"Date: 2012-02-02\n0800-0930 50% whatever ##TAG##\n0930-1215 work ##TAG2##\n",
		[]TimeRecord{
			{
				start:       time.Date(2012, 02, 02, 8, 0, 0, 0, time.Local),
				end:         time.Date(2012, 02, 02, 9, 30, 0, 0, time.Local),
				percent:     .5,
				description: "whatever",
				tags: []string{
					"TAG",
				},
			}, {
				start:       time.Date(2012, 02, 02, 9, 30, 0, 0, time.Local),
				end:         time.Date(2012, 02, 02, 12, 15, 0, 0, time.Local),
				percent:     1,
				description: "work",
				tags: []string{
					"TAG2",
				},
			},
		},
	},
}

func getRecords(test *parseTest, t *testing.T) []TimeRecord {
	r := strings.NewReader(test.input)
	records, err := Parse(r)
	if err != nil {
		t.Errorf("%+v", err)
	}
	return records
}

func TestParse(t *testing.T) {
	for _, test := range parseTests {
		records := getRecords(&test, t)
		if !reflect.DeepEqual(records, test.records) {
			t.Errorf("%s: got\n\t%v\nexpected\n\t%v", test.name, records, test.records)
		}
	}
}
