package main

import (
	"bitbucket.org/johnweldon/tabula_opera"
	"os"
)

func main() {
	tabula_opera.Format(os.Stdin, os.Stdout)
}
